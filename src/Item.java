public class Item {
	int key;
	Object value;
	Item next;

	public Item(int key, Object value) {
		this.key = key;
		this.value = value;
		next = null;
	}

	public Item() {
		next = null;
	}

	public int GetKey() {
		return key;
	}

	public Object GetKey1() {
		return key;
	}
	
	public Object GetValue() {
		return value;
	}

}
