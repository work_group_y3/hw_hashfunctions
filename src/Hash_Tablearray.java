public class Hash_Tablearray<T> {
	Item[] arrayHash;
	int table;

	public Hash_Tablearray(int table) {
		this.table = table;
		arrayHash = new Item[table];
		for (int i = 0; i < table; i++) {
			arrayHash[i] = new Item();
		}
	}

	int GetHash(int key) {
		return key % table;
	}

	public void put(int key, Object value) {
		int hashIndex = GetHash(key);
		Item arrayValue = arrayHash[hashIndex];
		Item newItem = new Item(key, value);
		newItem.next = arrayValue.next;
		arrayValue.next = newItem;
	}

	public T get(int key) {
		T value = null;
		int hashIndex = GetHash(key);
		Item arrayValue = arrayHash[hashIndex];

		while (arrayValue != null) {
			if (arrayValue.GetKey() == key) {
				value = (T) arrayValue.GetValue();
				break;
			}
			arrayValue = arrayValue.next;
		}
		return value;
	}
}
